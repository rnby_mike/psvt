
psvt  - Python SSL/TLS vulnerability tester
===========================================

Requirements:
=============

1. flextls >= 0.3
2. Python 3 (tested on 3.5)

Usage example:
==============
python psvt.py --report=term:rating=default --scan=server.ciphers --scan=vuln.heartbleed --scan=server.preferred_ciphers --scan=server.scsv --tls10 --tls11 --tls12 mail.ru yandex.ru