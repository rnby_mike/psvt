from psvt import modules, _helper as helper
from psvt.module.report import BaseReport


class Terminal(BaseReport):
    """
    Print results to the terminal.
    """

    name = "term"

    def __init__(self, **kwargs):
        BaseReport.__init__(self, **kwargs)
        self._rating = None
        self.color = helper.ColorConsole()

    def _print_custom(self, kb, base_id):
        kb_ids = kb.get_group_ids(base_id)
        if len(kb_ids) == 0:
            return

        for kb_id in kb_ids:
            item = kb.get(kb_id)
            print(item.label)

            sub_items = kb.get_list(kb_id)
            sub_ids = list(sub_items.keys())
            sub_ids.sort()
            for sub_id in sub_ids:
                sub_item = sub_items.get(sub_id)
                tmp_value = sub_item.value

                tmp_rating = self._rating.rate(sub_id, tmp_value)

                if isinstance(tmp_value, bool):
                    tmp_value = "yes" if tmp_value else "no"

                print(
                    "  {0}: {2}{1}{3}".format(
                        sub_item.label,
                        tmp_value,
                        helper.rating2color(self.color, tmp_rating),
                        self.color.RESET
                    )
                )
            print("")

    def _print_server_ciphers(self, kb):
        ciphers = kb.get("server.ciphers")
        if ciphers is None:
            return

        print("Supported Server Cipher(s):")
        for cipher in ciphers:
            rating_bits = self._rating.rate('cipher.bits', cipher.cipher_suite.bits, cipher)
            rating_protocol_version = self._rating.rate(
                'cipher.protocol_version',
                cipher.protocol_version,
                cipher
            )
            rating_name = self._rating.rate('cipher.name', cipher.cipher_suite.name, cipher)
            print(
                "  {0:9} {5}{1:7}{8} {6}{2:>9}{8} {7}{3}{8}  {4}".format(
                    cipher.status_name.capitalize(),
                    cipher.protocol_version_name,
                    "%d bits" % cipher.cipher_suite.bits,
                    cipher.cipher_suite.name,
                    "",  # reserved for alert info
                    helper.rating2color(self.color, rating_protocol_version),
                    helper.rating2color(self.color, rating_bits),
                    helper.rating2color(self.color, rating_name),
                    self.color.RESET
                )
            )
        print("")

    def _print_server_elliptic_curves(self, kb):
        curves = kb.get("server.ec.named_curves")
        if curves is not None:
            print("EC Named Curve(s):")
            for curve in curves:
                rating_protocol_version = self._rating.rate(
                    'ec.protocol_version',
                    curve.protocol_version,
                    curve
                )
                rating_name = self._rating.rate('ec.name', curve.elliptic_curve.name, curve)
                print(
                    "  {2}{0:7}{4} {3}{1}{4}".format(
                        curve.protocol_version_name,
                        curve.elliptic_curve.name,
                        helper.rating2color(self.color, rating_protocol_version),
                        helper.rating2color(self.color, rating_name),
                        self.color.RESET
                    )
                )
            print("")

        point_formats = kb.get("server.ec.point_formats")
        if point_formats is not None:
            print("EC Pointer Format(s):")
            for point_format in point_formats:
                print(
                    "  {1}{0}{2}".format(
                        point_format.name,
                        "",
                        self.color.RESET
                    )
                )
            print("")

    def _print_server_preferred_ciphers(self, kb):
        ciphers = kb.get("server.preferred_ciphers")
        if ciphers is None:
            return

        print("Preferred Server Cipher(s):")
        for cipher in ciphers:
            rating_version = self._rating.rate('cipher.protocol_version', cipher.protocol_version, cipher)
            if cipher.cipher_suite is None:
                print(
                    "  {1}{0:7}{2} Protocol version not supported".format(
                        cipher.protocol_version_name,
                        helper.rating2color(self.color, rating_version),
                        self.color.RESET
                    )
                )
                continue

            if cipher.cipher_suite is False:
                print(
                    "  {1}{0:7}{2} No preferred cipher suite".format(
                        cipher.protocol_version_name,
                        helper.rating2color(self.color, rating_version),
                        self.color.RESET
                    )
                )
                continue

            rating_bits = self._rating.rate('cipher.bits', cipher.cipher_suite.bits, cipher)
            rating_name = self._rating.rate('cipher.name', cipher.cipher_suite.name, cipher)
            print(
                "  {4}{0:7}{7} {5}{1:>9}{7} {6}{2}{7} {3}".format(
                    cipher.protocol_version_name,
                    "%d bits" % cipher.cipher_suite.bits,
                    cipher.cipher_suite.name,
                    "",  # alert info
                    helper.rating2color(self.color, rating_version),
                    helper.rating2color(self.color, rating_bits),
                    helper.rating2color(self.color, rating_name),
                    self.color.RESET
                )
            )
        print("")

    def _print_server_security(self, kb):
        if len(kb.get_list("server.security.")) == 0:
            return

        print("Security:")
        scsv_supported = kb.get("server.security.scsv", "")
        if scsv_supported != "":
            rating_scsv = self._rating.rate(
                "server.security.scsv",
                scsv_supported
            )
            tmp_value = "-"
            if scsv_supported is None:
                tmp_value = "unkown"
            elif scsv_supported is True:
                tmp_value = "supported"
            elif scsv_supported is False:
                tmp_value = "not supported"

            print(
                "  Signaling Cipher Suite Value (SCSV): {1}{0}{2}".format(
                    tmp_value,
                    helper.rating2color(self.color, rating_scsv),
                    self.color.RESET
                )
            )

        print("")

    def _print_server_session(self, kb):
        if len(kb.get_list("server.session.")) == 0:
            return

        print("Session:")
        compression = kb.get("server.session.compression")
        rating_compression = self._rating.rate(
            "server.session.compression",
            compression
        )
        if compression is not None:
            if not compression:
                compression_name = "None"
            else:
                compression_name = compression.name
            print(
                "  Compression: {1}{0}{2}".format(
                    compression_name,
                    helper.rating2color(self.color, rating_compression),
                    self.color.RESET
                )
            )

        expansion = kb.get("server.session.expansion")
        rating_expansion = self._rating.rate(
            "server.session.expansion",
            expansion
        )
        if expansion is not None:
            if not expansion:
                expansion = "None"
            print(
                "  Expansion: {0}".format(
                    expansion,
                    helper.rating2color(self.color, rating_expansion),
                    self.color.RESET
                )
            )

        print("")

    def run(self):
        kb = self._scanner.get_knowledge_base()
        handler = self._scanner.get_handler()

        print("")
        print(handler.hostname)
        print("=" * len(handler.hostname))
        print("")

        self._rating = self._scanner.load_rating(self.config.get_value("rating"))

        self._print_server_ciphers(kb)

        self._print_server_preferred_ciphers(kb)

        self._print_server_security(kb)

        self._print_server_session(kb)

        self._print_server_elliptic_curves(kb)

        self._print_custom(kb, "vulnerability.custom")


modules.register(Terminal)
