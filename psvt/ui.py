import argparse

from psvt import Scanner, modules
from psvt.module.report import BaseReport
from psvt.module.scan import BaseScan


def run():
    global modules
    parser = argparse.ArgumentParser(description="psvt")
    parser.add_argument("--report", action="append", default=[])
    parser.add_argument("--scan", action="append", default=[])
    parser.add_argument("host_uris",
                        metavar="HOSTURI",
                        nargs="+",
                        help="Hosts to scan"
                        )

    for name, opt_args in Scanner.config_options:
        parser.add_argument(
            "--%s" % name,
            action='store_true',
            default=opt_args.get('default'),
            dest=name
        )

    args = parser.parse_args()
    modules.load_global_modules()
    scanner = Scanner()
    args_dict = vars(args)
    for name, opt_args in Scanner.config_options:
        if name in args_dict:
            scanner.config.set_value(name, args_dict.get(name))

    for module in args.scan:
        name, sep, options = module.partition(":")
        scanner.append_load(name, options, base_class=BaseScan)

    for module in args.report:
        name, sep, options = module.partition(":")
        scanner.append_load(name, options, base_class=BaseReport)

    for host_uri in args.host_uris:
        module = scanner.load_handler_from_uri(host_uri)
        scanner.set_handler(module)
        scanner.run()

